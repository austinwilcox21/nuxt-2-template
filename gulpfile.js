const gulp = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const cssnano = require("gulp-cssnano");
const rename = require("gulp-rename");
const notify = require("gulp-notify");
const sass = require("gulp-sass")(require('sass'));

function styles() {
  return gulp
    .src("styles/scss/main.scss")
    .pipe(sass())
    .pipe(gulp.dest("styles/css/"))
    .pipe(autoprefixer())
    .pipe(rename({ suffix: ".min" }))
    .pipe(cssnano())
    .pipe(gulp.dest("styles/css/"))
    .pipe(notify({ message: "Styles task complete" }));
}

gulp.task("styles", () => {
  return styles();
});

gulp.task("watch:styles", function() {
  gulp.watch("styles/scss/*/*.scss", function() {
    return styles();
  });
});

